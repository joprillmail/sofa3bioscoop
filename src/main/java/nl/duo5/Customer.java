package nl.duo5;

import nl.duo5.order.Order;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private final List<Order> orders;

    public Customer() {
        this.orders = new ArrayList<>();
    }

    public void addOrder(Order order) {
        this.orders.add(order);
    }

    public List<Order> getOrders() {
        return orders;
    }
}
