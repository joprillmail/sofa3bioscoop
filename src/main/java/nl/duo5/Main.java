package nl.duo5;

import nl.duo5.order.Order;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        List<Movie> movies = new ArrayList<>();
        List<Order> orders = new ArrayList<>();

        // Create movies
        movies.add(new Movie("Final Order of Zepherus"));
        movies.add(new Movie("Superman"));

        // Create screenings
        movies.get(0).addScreening(new MovieScreening(movies.get(0), LocalDateTime.now(), 11.50));
        movies.get(0).addScreening(new MovieScreening(movies.get(0), LocalDateTime.now().plusDays(1), 11.50));
        movies.get(1).addScreening(new MovieScreening(movies.get(1), LocalDateTime.now().plusDays(2), 14.50));

        // Create customer
        var customer = new Customer();

        var order = new Order();
        order.selectShow(movies);
        order.selectShow(movies);

        order.amountTickets();
        order.selectShow(movies);
        order.amountTickets();

        order.isStudentOrder();
        order.selectShow(movies);
        order.amountTickets();
        order.isStudentOrder();

        order.parkingTicket();
        order.selectShow(movies);
        order.amountTickets();
        order.parkingTicket();

        order.reserveSeats();
        order.selectShow(movies);
        order.amountTickets();
        order.parkingTicket();
        order.reserveSeats();

        order.submit(customer);

        System.out.println("Amount orders: " + customer.getOrders().size());
    }
}
