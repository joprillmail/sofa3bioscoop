package nl.duo5;

import java.time.LocalDateTime;

public class MovieScreening {
    private final Movie movie;
    private final LocalDateTime dateAndTime;
    private final double pricePerSeat;

    public MovieScreening(Movie movie, LocalDateTime dateAndTime, double pricePerSeat){
        this.dateAndTime = dateAndTime;
        this.pricePerSeat = pricePerSeat;
        this.movie = movie;
    }

    public double getPricePerSeat() {
        return this.pricePerSeat;
    }

    public LocalDateTime getDateAndTime() {
        return dateAndTime;
    }

    @Override
    public String toString() {
        return "{" + '\n' +
                "   \"movie\": " + movie + ",\n" +
                "   \"dateAndTime\": \"" + dateAndTime + "\",\n" +
                "   \"pricePerSeat\": " + pricePerSeat + '\n' +
                '}';
    }
}
