package nl.duo5;

import java.util.ArrayList;
import java.util.List;

public class Movie {
    private final String title;
    private final List<MovieScreening> screenings = new ArrayList<>();

    public Movie(String title){
        this.title = title;
    }

    public String getTitle(){
        return this.title;
    }

    public void addScreening(MovieScreening screening){
        this.screenings.add(screening);
    }

    public List<MovieScreening> getScreenings(){
        return this.screenings;
    }

    @Override
    public String toString() {
        return "{" +
                "\"title\": \"" + title + '\"' + //",\n" +
                //"   screenings: " + screenings +
                '}';
    }
}
