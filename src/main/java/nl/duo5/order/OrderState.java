package nl.duo5.order;

import nl.duo5.Customer;
import nl.duo5.Movie;
import nl.duo5.MovieScreening;
import nl.duo5.MovieTicket;

import java.util.List;

public interface OrderState {
    void selectShow(List<Movie> movies);
    void amountTickets(MovieScreening screening);
    void isStudentOrder();
    void parkingTicket();
    void reserveSeats();
    void submit(Customer customer);
}
