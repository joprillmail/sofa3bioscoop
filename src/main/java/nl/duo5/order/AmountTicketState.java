package nl.duo5.order;

import nl.duo5.Customer;
import nl.duo5.Movie;
import nl.duo5.MovieScreening;
import nl.duo5.MovieTicket;

import java.util.List;
import java.util.Scanner;

public class AmountTicketState implements OrderState{
    private final Order order;

    public AmountTicketState(Order order) {
        this.order = order;
    }

    @Override
    public void selectShow(List<Movie> movies) {
        System.out.println("Show was already selected!");
    }

    @Override
    public void amountTickets(MovieScreening screening) {
        Scanner reader = new Scanner(System.in);
        System.out.print("Select the amount of tickets: ");
        var amountTickets = Integer.parseInt(reader.nextLine());
        order.setAmount(amountTickets);
        order.setState(order.getStudentState());
    }

    @Override
    public void isStudentOrder() {
        System.out.println("You have to select the amount of tickets first");
    }

    @Override
    public void parkingTicket() {
        System.out.println("You have to select the amount of tickets first");
    }

    @Override
    public void reserveSeats() {
        System.out.println("You have to select the amount of tickets first");
    }

    @Override
    public void submit(Customer customer) {
        System.out.println("Not everything has been determined yet");
    }
}
