package nl.duo5.order;

import nl.duo5.Customer;
import nl.duo5.Movie;
import nl.duo5.MovieScreening;
import nl.duo5.MovieTicket;

import java.util.List;
import java.util.Scanner;

public class SelectShowState implements OrderState {
    private final Order order;

    public SelectShowState(Order order) {
        this.order = order;
    }

    @Override
    public void selectShow(List<Movie> movies) {
        Scanner reader = new Scanner(System.in);
        int movieID;
        int screeningID = -1;
        for (int i = 0; i < movies.size(); i++){
            System.out.println(i + ". [" + movies.get(i).getTitle() + "]");
        }
        System.out.print("Type the corresponding ID of the Movie: ");
        movieID = Integer.parseInt(reader.nextLine());

        if (movies.get(movieID).getScreenings().size() > 0){
            for (int i = 0; i < movies.get(movieID).getScreenings().size(); i++){
                System.out.println(i + ". [" + movies.get(movieID).getScreenings().get(i).getDateAndTime() + "]");
            }
            System.out.print("Type the corresponding ID of the Screening: ");
            screeningID = Integer.parseInt(reader.nextLine());
        } else {
            System.out.println("This movie does not have any screenings. Please select another one");
            this.selectShow(movies);
        }

        if (screeningID == -1){
            System.out.println("Something went wrong!");
        } else {
            var screening = movies.get(movieID).getScreenings().get(screeningID);
            order.setScreening(screening);
            order.setState(order.getAmountTicketState());
        }
    }

    @Override
    public void amountTickets(MovieScreening screening) {
        System.out.println("You first have to select a show");
    }

    @Override
    public void isStudentOrder() {
        System.out.println("You first have to select a show");
    }

    @Override
    public void parkingTicket() {
        System.out.println("You first have to select a show");
    }

    @Override
    public void reserveSeats() {
        System.out.println("You first have to select a show");
    }

    @Override
    public void submit(Customer customer) {
        System.out.println("Not everything has been determined yet");
    }
}
