package nl.duo5.order;

import nl.duo5.Customer;
import nl.duo5.Movie;
import nl.duo5.MovieScreening;
import nl.duo5.MovieTicket;

import java.util.List;
import java.util.Scanner;

public class ParkingTicketState implements OrderState {
    private final Order order;

    public ParkingTicketState(Order order) {
        this.order = order;
    }

    @Override
    public void selectShow(List<Movie> movies) {
        System.out.println("Movie has already be selected");
    }

    @Override
    public void amountTickets(MovieScreening screening) {
        System.out.println("Amount of tickets have already been determined");
    }

    @Override
    public void isStudentOrder() {
        System.out.println("Is student order has already been determined");
    }

    @Override
    public void parkingTicket() {
        Scanner reader = new Scanner(System.in);
        System.out.print("Do you want a parking ticket? [Y/N] ");
        var parkingTicket = reader.nextLine();
        if (parkingTicket.equalsIgnoreCase("Y")) order.setParkingTicket(true);
        else if (parkingTicket.equalsIgnoreCase("N")) order.setParkingTicket(false);
        else {
            System.out.println("This is not a valid input type, try again!");
            this.parkingTicket();
        }
        order.setState(order.getReserveSeatsState());
    }

    @Override
    public void reserveSeats() {
        System.out.println("You first have to select if you want a parking ticket");
    }

    @Override
    public void submit(Customer customer) {
        System.out.println("Not everything has been determined yet");
    }

}
