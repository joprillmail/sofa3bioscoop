package nl.duo5.order;

import nl.duo5.Customer;
import nl.duo5.Movie;
import nl.duo5.MovieScreening;
import nl.duo5.MovieTicket;

import java.util.List;

public class SubmitState implements OrderState {
    private final Order order;

    public SubmitState(Order order) {
        this.order = order;
    }

    @Override
    public void selectShow(List<Movie> movies) {
        System.out.println("Show has already been determined");
    }

    @Override
    public void amountTickets(MovieScreening screening) {
        System.out.println("Has already been determined");

    }

    @Override
    public void parkingTicket() {
        System.out.println("Amount of tickets has already been determined");
    }

    @Override
    public void isStudentOrder() {

    }

    @Override
    public void reserveSeats() {
        System.out.println("Seats are ready to be reserved");
    }

    @Override
    public void submit(Customer customer) {
        customer.addOrder(order);
    }
}
