package nl.duo5.order;

import nl.duo5.Customer;
import nl.duo5.Movie;
import nl.duo5.MovieScreening;
import nl.duo5.MovieTicket;

import java.util.ArrayList;
import java.util.List;

public class Order {
    private MovieScreening screening;
    private int amount;
    private boolean studentOrder;
    private boolean parkingTicket;
    private List<MovieTicket> tickets = new ArrayList<>();

    private final OrderState selectShowState;
    private final OrderState amountTicketState;
    private final OrderState studentState;
    private final OrderState parkingTicketState;
    private final OrderState reserveSeatsState;
    private final OrderState submitState;

    private OrderState orderState;

    public Order() {
        this.selectShowState = new SelectShowState(this);
        this.amountTicketState = new AmountTicketState(this);
        this.studentState = new StudentState(this);
        this.parkingTicketState = new ParkingTicketState(this);
        this.reserveSeatsState = new ReserveSeatsState(this);
        this.submitState = new SubmitState(this);

        this.orderState = selectShowState;
    }

    public void selectShow(List<Movie> movies){
        orderState.selectShow(movies);
    }

    public void amountTickets(){
        orderState.amountTickets(this.screening);
    }

    public void isStudentOrder() {
        orderState.isStudentOrder();
    }

    public void parkingTicket(){
        orderState.parkingTicket();
    }

    public void reserveSeats(){
        orderState.reserveSeats();
    }

    public void submit(Customer customer) {
        orderState.submit(customer);
    }

    public OrderState getSelectShowState() {
        return selectShowState;
    }

    public OrderState getAmountTicketState() {
        return amountTicketState;
    }

    public OrderState getStudentState(){
        return studentState;
    }

    public OrderState getParkingTicketState() {
        return parkingTicketState;
    }

    public OrderState getReserveSeatsState() {
        return reserveSeatsState;
    }

    public OrderState getSubmitState() {
        return submitState;
    }

    public void setState(OrderState state){
        this.orderState = state;
    }

    public OrderState statusState(){
        return this.orderState;
    }

    public void setScreening(MovieScreening screening) {
        this.screening = screening;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setStudentOrder(boolean studentOrder){
        this.studentOrder = studentOrder;
    }

    public void setParkingTicket(boolean parkingTicket) {
        this.parkingTicket = parkingTicket;
    }

    public void setTickets(List<MovieTicket> tickets) {
        this.tickets = tickets;
    }

    public MovieScreening getScreening() {
        return screening;
    }

    public int getAmount() {
        return amount;
    }

    public boolean isParkingTicket() {
        return parkingTicket;
    }
}
