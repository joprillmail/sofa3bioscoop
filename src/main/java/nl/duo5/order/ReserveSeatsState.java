package nl.duo5.order;

import nl.duo5.Customer;
import nl.duo5.Movie;
import nl.duo5.MovieScreening;
import nl.duo5.MovieTicket;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReserveSeatsState implements OrderState {
    private final Order order;

    public ReserveSeatsState(Order order) {
        this.order = order;
    }

    @Override
    public void selectShow(List<Movie> movies) {
        System.out.println("Show has already been selected");
    }

    @Override
    public void amountTickets(MovieScreening screening) {
        System.out.println("Amount of tickets has already been determined");
    }

    @Override
    public void isStudentOrder() {
        System.out.println("Is student order has already been determined");
    }

    @Override
    public void parkingTicket() {
        System.out.println("Parking ticket has already been determined");
    }

    @Override
    public void reserveSeats() {
        List<MovieTicket> tickets = new ArrayList<>();
        Scanner reader = new Scanner(System.in);
        for (int i = 0; i < order.getAmount(); i++){
            System.out.println("-- TICKET " + (i+1) + " / " + order.getAmount() + " --");
            System.out.print("Enter rowNr: ");
            var rowNr = Integer.parseInt(reader.nextLine());
            System.out.print("Enter seatNr: ");
            var seatNr = Integer.parseInt(reader.nextLine());
            System.out.print("Premium Seats? [true/false] ");
            var premium = Boolean.parseBoolean(reader.nextLine());
            tickets.add(new MovieTicket(order.getScreening(), premium, rowNr, seatNr));
        }

        order.setTickets(tickets);
        order.setState(order.getSubmitState());
    }

    @Override
    public void submit(Customer customer) {
        System.out.println("Not everything has been determined yet");
    }
}
