package nl.duo5.order;

import nl.duo5.Customer;
import nl.duo5.Movie;
import nl.duo5.MovieScreening;

import java.util.List;
import java.util.Scanner;

public class StudentState implements OrderState {
    private Order order;

    public StudentState(Order order) {
        this.order = order;
    }


    @Override
    public void selectShow(List<Movie> movies) {
        System.out.println("Show has already been selected");
    }

    @Override
    public void amountTickets(MovieScreening screening) {
        System.out.println("Screening has already been determined");
    }

    @Override
    public void isStudentOrder() {
        Scanner reader = new Scanner(System.in);
        System.out.print("Is the order a Student order? [Y/N] ");
        var isStudentOrder = reader.nextLine();

        if (isStudentOrder.equalsIgnoreCase("Y")) order.setStudentOrder(true);
        else if (isStudentOrder.equalsIgnoreCase("N")) order.setStudentOrder(false);
        else this.isStudentOrder();

        order.setState(order.getParkingTicketState());
    }

    @Override
    public void parkingTicket() {

    }

    @Override
    public void reserveSeats() {

    }

    @Override
    public void submit(Customer customer) {

    }
}
