package nl.duo5;

public class MovieTicket {
    private final MovieScreening screening;
    private final int rowNr;
    private final int seatNr;
    private final boolean isPremium;

    public MovieTicket(MovieScreening movieScreening, boolean isPremiumReservation, int rowNr, int seatNr) {
        this.rowNr = rowNr;
        this.seatNr = seatNr;
        this.isPremium = isPremiumReservation;
        this.screening = movieScreening;
    }

    public boolean isPremiumTicket() {
        return this.isPremium;
    }

    public double getPrice() {
        return this.screening.getPricePerSeat();
    }

    public MovieScreening getScreening() {
        return screening;
    }

    @Override
    public String toString() {
        return "{" + '\n' +
                "   \"screening\": " + screening + ",\n" +
                "   \"rowNr\": " + rowNr + ",\n" +
                "   \"seatNr\": " + seatNr + ",\n" +
                "   \"isPremium\": " + isPremium + '\n' +
                '}';
    }
}
