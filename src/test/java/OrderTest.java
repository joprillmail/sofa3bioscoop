import nl.duo5.Customer;
import nl.duo5.Movie;
import nl.duo5.MovieScreening;
import nl.duo5.order.Order;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OrderTest {
    List<Movie> movies = new ArrayList<>();
    double pricePerSeatMovieOne = 11.50;
    double pricePerSeatMovieTwo = 14.50;

    @Before
    public void init() {
        movies.add(new Movie("Final nl.duo5.Order of Zepherus"));
        movies.add(new Movie("Superman"));

        movies.get(0).addScreening(new MovieScreening(movies.get(0), LocalDateTime.of(2022, 2, 6, 12, 00), pricePerSeatMovieOne));  // Zondag
        movies.get(1).addScreening(new MovieScreening(movies.get(1), LocalDateTime.of(2022, 2, 12, 21, 50), pricePerSeatMovieTwo)); // Zaterdag
        movies.get(1).addScreening(new MovieScreening(movies.get(1), LocalDateTime.of(2022, 2, 8, 17, 30), pricePerSeatMovieTwo));  // Dinsdag
    }

    @Test
    public void TestIfOrderCanBeMadeSuccessful(){
        // Arrange
        var order = new Order();
        var customer = new Customer();

        // Act
        var input = "0\n0";
        InputStream ioStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        System.setIn(ioStream);
        order.selectShow(movies);

        input = "1";
        ioStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        System.setIn(ioStream);
        order.amountTickets();

        input = "N";
        ioStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        System.setIn(ioStream);
        order.isStudentOrder();

        input = "N";
        ioStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        System.setIn(ioStream);
        order.parkingTicket();

        input = "1\n1\ntrue";
        ioStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        System.setIn(ioStream);
        order.reserveSeats();

        order.submit(customer);

        // Assert
        Assert.assertEquals(1, customer.getOrders().size());
    }

    @Test
    public void TestIfAllPossibleOptionsAreHit(){
        // Arrange
        var order = new Order();
        var customer = new Customer();

        // Act
        var input = "0\n0";
        InputStream ioStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        System.setIn(ioStream);
        order.selectShow(movies);
        order.selectShow(movies);

        input = "2";
        ioStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        System.setIn(ioStream);
        order.amountTickets();
        order.selectShow(movies);
        order.amountTickets();

        input = "Y";
        ioStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        System.setIn(ioStream);
        order.isStudentOrder();

        input = "N";
        ioStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        System.setIn(ioStream);
        order.parkingTicket();
        order.selectShow(movies);
        order.amountTickets();
        order.parkingTicket();

        input = "1\n1\ntrue\n1\n1\ntrue";
        ioStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        System.setIn(ioStream);
        order.reserveSeats();
        order.selectShow(movies);
        order.amountTickets();
        order.parkingTicket();
        order.reserveSeats();

        order.submit(customer);

        // Assert
        Assert.assertEquals(2, order.getAmount());
        Assert.assertEquals(1, customer.getOrders().size());
    }
}
